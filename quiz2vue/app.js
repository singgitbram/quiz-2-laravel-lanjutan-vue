var app = new Vue({
  el: "#app",
  data: {
    users: [
      { name: "Muhammad Iqbal Mubarok" },
      { name: "Ruby Purwanti" },
      { name: "Faqih Muhammad" },
    ],
    showEditForm: false,
    newUserName: "",
    editUserName: "",
    indexUntukEdit: "",
  },

  methods: {
    addUser: function () {
      let obj = {
        name: this.newUserName,
      };
      this.users.push(obj);
      this.newUserName = "";
    },
    editUserPressed: function (index) {
      this.showEditForm = true;
      this.indexUntukEdit = index;
      this.editUserName = this.users[index].name;
    },
    editUserConfirm: function () {
      this.users[this.indexUntukEdit].name = this.editUserName;
      this.editUserName = "";
      this.indexUntukEdit = "";
      this.showEditForm = false;
    },
    removeUser: function (index) {
      if (confirm("Anda Yakin?")) {
        this.users.splice(index, 1);
      }
    },
  },
});
