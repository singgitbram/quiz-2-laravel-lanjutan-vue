<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <div id="app">
        <h3>Visitor List</h3>
        <input v-if="!showEditForm" type="text" v-model="newUserName" />
        <button v-if="!showEditForm" @click="addUser">Add</button>
        <input v-if="showEditForm" type="text" v-model="editUserName" />
        <button v-if="showEditForm" @click="editUserConfirm">Update</button>
        <ul>
            <li v-for="(visitor, index) in visitors">
                <span>@{{ visitor . name }}</span>
                <button @click="editUserPressed(index, visitor)">Edit</button> ||
                <button @click="removeUser(index, visitor)">Delete</button>
            </li>
        </ul>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        new Vue({
            el: "#app",
            data: {
                visitors: [],
                showEditForm: false,
                newUserName: "",
                editUserName: "",
                indexUntukEdit: "",
                idUntukEdit: ""
            },
            methods: {
                addUser: function() {
                    let obj = {
                        name: this.newUserName
                    }
                    if (this.newUserName) {
                        axios.post('/api/visitor', {
                            name: this.newUserName
                        }).then(response => {
                            this.visitors.push(obj);
                            this.newUserName = "";
                        }).then(response => {
                            axios.get('/api/visitor').then(response => {
                                let result = response.data.data
                                this.visitors = result
                            });
                        })
                    }
                },
                editUserPressed: function(index, visitor) {
                    this.showEditForm = true;
                    this.indexUntukEdit = index;
                    this.idUntukEdit = visitor.id;
                    this.editUserName = this.visitors[index].name;
                },
                editUserConfirm: function() {
                    axios.post('/api/visitor/update/' + this.idUntukEdit, {
                        name: this.editUserName
                    }).then(response => {
                        this.visitors[this.indexUntukEdit].name = this.editUserName;
                        this.editUserName = "";
                        this.indexUntukEdit = "";
                        this.idUntukEdit = ""
                        this.showEditForm = false;
                    });
                },
                removeUser: function(index, visitor) {
                    if (confirm("Anda Yakin?")) {
                        axios.delete('/api/visitor/delete/' + visitor.id).then(response => {
                            this.visitors.splice(index, 1);
                        });
                    }
                },
            },
            mounted: function() {
                axios.get('/api/visitor').then(response => {
                    let result = response.data.data
                    this.visitors = result
                });
            },
        })

    </script>
</body>

</html>
