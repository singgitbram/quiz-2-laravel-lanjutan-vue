<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('visitor', 'VisitorController@index');
Route::post('visitor', 'VisitorController@store');
Route::delete('visitor/delete/{id}', 'VisitorController@destroy');
Route::get('visitor/show/{id}', 'VisitorController@show');
Route::post('visitor/update/{id}', 'VisitorController@update');
