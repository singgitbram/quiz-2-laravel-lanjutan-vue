<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visitor;
use App\Http\Resources\VisitorResource; //ini supaya keluarnya sesuai kolom yg kita mau

class VisitorController extends Controller
{
    public function index()
    {
        $visitors = Visitor::all();
        // $visitors  Visitor::orderBy('created_at','desc')->get();ini kl mau dapet dr data paling baru
        return VisitorResource::collection($visitors);
    }

    public function store(Request $request)
    {
        $visitor = Visitor::create([
            'name' => $request->name
        ]);
        return new VisitorResource($visitor);
    }

    public function destroy($id)
    {
        Visitor::destroy($id);
        return 'success';
    }

    public function show($id)
    {
        $visitor = Visitor::find($id);
        return new VisitorResource($visitor);
    }

    public function update(Request $request, $id)
    {
        $visitor = Visitor::find($id);
        $visitor->update([
            'name' => $request->name
        ]);
        return new VisitorResource($visitor);
    }
}
